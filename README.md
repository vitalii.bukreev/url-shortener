### Url shortener 
Shortens long URL so it’s ready to be shared everywhere

Check this app here: https://url-fb-shortener.herokuapp.com/

### Built With
- Firebase Dynamic Links API

### Getting Started

- `npm install`
- `bower install`
- Run `gulp serve` to preview and watch for changes
- Run `gulp serve --port=8080` to preview and watch for changes in port `8080`
- Run `gulp` to build your webapp for production
- Run `gulp serve:dist` to preview the production build
- Run `gulp serve:dist --port=5000` to preview the production build in port `5000`
