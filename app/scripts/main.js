$(document).ready(function(){
	
	var copyButton = '<span id="copyButton" class="copy-icon"><i class="far fa-copy"></i></span>'; 

	var longUrl = '';

	function getTinyUrl(longUrl){
					
		$.ajax({
			url: 'https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyDIjtP18HWftMtbWsOLgd1wYsQKqiSOK7k', 
			type: 'POST',
			contentType: 'application/json; charset=utf-8',
			data: '{longDynamicLink: "https://fbls.page.link/?link=' + longUrl +'", suffix: {option: "SHORT"}}',
			dataType: 'json',
			success: function(response){
				var tinyUrl = response.shortLink;
				console.log(tinyUrl);
				$('.urlholder').text(tinyUrl);
				$('.urlholder').append(copyButton);
				$('#copyButton').click(function(){
			    	var $temp = $('<input>');
			    	$('body').append($temp);
			    	$temp.val($('.urlholder').text()).select();
			    	document.execCommand('copy');
			    	$temp.remove();
    			})
			},
			error: function(response) {
				if (response.code != 200) {
					$('.urlholder').text('Sorry, something go wrong');
				} 	  
			}
		});			
	}

	$('form').on('submit', function(){
		longUrl = $(this).serializeArray()[0].value;
    	event.preventDefault();
    	console.log(longUrl);
    	getTinyUrl(longUrl);
    });

});

	
